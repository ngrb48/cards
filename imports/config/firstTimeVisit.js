//Settings for first time visitors
let enableFirstTimeVisitDemo = false;
let enableFirstTimeVisitModal = false;
let redirectToHomeAfterExit = true;
let enableFirstTimePresentationModal = false;

module.exports = {
	enableFirstTimeVisitDemo,
	enableFirstTimeVisitModal,
	redirectToHomeAfterExit,
	enableFirstTimePresentationModal
};
